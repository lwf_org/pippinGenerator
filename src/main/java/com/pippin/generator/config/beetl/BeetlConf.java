package com.pippin.generator.config.beetl;

import com.pippin.generator.config.beetl.ext.fn.Substring;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import javax.annotation.Resource;

/**
 * web配置类
 * @author : Natus
 * @date : 2020/7/27 0027
 */

@Configuration
public class BeetlConf {

    @Resource
    private BeetlProperties beetlProperties;

    /**
     * beetl的配置
     */
    @Bean(name = "beetlConfig")
    public BeetlConfiguration getBeetlConfiguration() {
        BeetlConfiguration beetlConfiguration = new BeetlConfiguration();
        beetlConfiguration.setResourceLoader(new ClasspathResourceLoader(BeetlConf.class.getClassLoader(), beetlProperties.getPrefix()));
        beetlConfiguration.setConfigProperties(beetlProperties.getProperties());
        beetlConfiguration.init();
        return beetlConfiguration;
    }

    /**
     * beetl的视图解析器
     */
    @Bean(name = "beetlViewResolver")
    public BeetlSpringViewResolver beetlViewResolver(@Qualifier("beetlConfig") BeetlConfiguration beetlConfiguration) {
        BeetlSpringViewResolver beetlSpringViewResolver = new BeetlSpringViewResolver();
        beetlSpringViewResolver.setConfig(beetlConfiguration);
        beetlSpringViewResolver.setContentType("text/html;charset=UTF-8");
        beetlSpringViewResolver.setOrder(0);
        return beetlSpringViewResolver;
    }
}
