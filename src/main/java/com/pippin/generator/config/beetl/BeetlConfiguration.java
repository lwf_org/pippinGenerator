package com.pippin.generator.config.beetl;


import com.pippin.generator.config.beetl.ext.fn.LowerCase;
import com.pippin.generator.config.beetl.ext.fn.Substring;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.springframework.stereotype.Component;

/**
 * beetl拓展配置,绑定一些工具类,方便在模板中直接调用
 *
 * @author : Natus
 * @date : 2020/7/27 0027
 */
@Component
public class BeetlConfiguration extends BeetlGroupUtilConfiguration {


    @Override
    public void initOther() {
        this.groupTemplate.registerFunction("substring", new Substring());
        this.groupTemplate.registerFunction("lowerCase", new LowerCase());
    }
}
