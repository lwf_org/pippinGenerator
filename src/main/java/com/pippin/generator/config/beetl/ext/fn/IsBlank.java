package com.pippin.generator.config.beetl.ext.fn;

import cn.hutool.core.util.StrUtil;
import org.beetl.core.Context;
import org.beetl.core.Function;
import org.beetl.ext.fn.EmptyExpressionFunction;

/**
 * web配置类
 * @author : Natus
 * @date : 2020/7/27 0027
 */
public class IsBlank implements Function {
	
	EmptyExpressionFunction fn = new EmptyExpressionFunction();
	
	@Override
	public Object call(Object[] paras, Context ctx) {
		Object a = paras[0];
		if (a == null) {
			return true;
		} else {
			if (a instanceof String) {
				return StrUtil.isBlank((String)a);
			} else {
				return this.fn.call(paras, ctx);
			}
		}
	}
}
