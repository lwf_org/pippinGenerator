package com.pippin.generator.config.beetl.ext.fn;

import org.beetl.core.Context;
import org.beetl.core.Function;

/**
 * web配置类
 *
 * @author : Natus
 * @date : 2020/7/27 0027
 */
public class Substring implements Function {

    @Override
    public Object call(Object[] objects, Context context) {
        if (objects.length == 1) {
            return objects[0];
        } else if (objects.length == 2) {
            String str = String.valueOf(objects[0]);
            Integer i = (Integer) objects[1];
            return str.substring(i);
        } else if (objects.length > 2) {
            String str = String.valueOf(objects[0]);
            Integer i = (Integer) objects[1];
            Integer j = (Integer) objects[2];
            return str.substring(i, j);
        }
        return null;
    }
}
