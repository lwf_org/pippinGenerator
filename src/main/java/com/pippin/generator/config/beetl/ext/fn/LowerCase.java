package com.pippin.generator.config.beetl.ext.fn;

import org.beetl.core.Context;
import org.beetl.core.Function;

public class LowerCase implements Function {
    @Override
    public Object call(Object[] objects, Context context) {
        return String.valueOf(objects[0]).toLowerCase();
    }
}
