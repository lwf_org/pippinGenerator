package com.pippin.generator.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Description:
 * Author: Natsu.Luo 2020/2/7
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    /**
     * 静态资源映射
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");

        //本应用
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");

    }

}
