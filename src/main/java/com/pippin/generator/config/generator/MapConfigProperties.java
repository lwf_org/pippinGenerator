package com.pippin.generator.config.generator;

import com.pippin.generator.entity.Db;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author : Natus
 * @date : 2020/7/27 0027
 */
public class MapConfigProperties {

    public static Map<String, Db> dbMap = new ConcurrentHashMap<>();

    public static Map<String, GlobalConfigProperties> globalMap = new HashMap<>();

    public static Map<String, PackageConfigProperties> packageMap = new HashMap<>();

    public static Map<String, StrategyConfigProperties> strategyMap = new HashMap<>();

    public static Map<String, OtherConfigProperties> otherMap = new HashMap<>();


    public static void initProperties(String hashCode){
        packageMap.put(hashCode, new PackageConfigProperties());
        strategyMap.put(hashCode, new StrategyConfigProperties());
        otherMap.put(hashCode, new OtherConfigProperties());
        globalMap.put(hashCode, new GlobalConfigProperties());
    }



}
