package com.pippin.generator.config.generator;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import lombok.Data;

import java.util.Map;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
@Data
public class PackageConfigProperties {

    /**
     * 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
     */
    private String parent = "com.pippin.backstage";
    /**
     * 父包模块名
     */
    private String moduleName;
    /**
     * Entity包名
     */
    private String entity = "entity";
    /**
     * Service包名
     */
    private String service = "service";
    /**
     * Service Impl包名
     */
    private String serviceImpl = "service.impl";
    /**
     * Mapper包名
     */
    private String mapper = "mapper";
    /**
     * Mapper XML包名
     */
    private String xml = "mapper.xml";
    /**
     * Controller包名
     */
    private String controller = "controller";
    /**
     * 路径配置信息
     */
    private Map<String, String> pathInfo;

    public PackageConfig built() {
        return new PackageConfig()
                .setController(this.controller)
                .setXml(this.xml)
                .setEntity(entity)
                .setMapper(mapper)
                .setParent(parent)
                .setServiceImpl(serviceImpl)
                .setService(service)
                .setPathInfo(pathInfo);
    }

    public static PackageConfigProperties change(PackageConfigProperties packageConfigProperties){
        PackageConfigProperties defaultValue = new PackageConfigProperties();
        packageConfigProperties.parent = StrUtil.isBlank(packageConfigProperties.parent)? defaultValue.parent:packageConfigProperties.parent;
        packageConfigProperties.controller = StrUtil.isBlank(packageConfigProperties.controller)? defaultValue.controller:packageConfigProperties.controller;
        packageConfigProperties.entity = StrUtil.isBlank(packageConfigProperties.entity)? defaultValue.entity:packageConfigProperties.entity;
        packageConfigProperties.xml = StrUtil.isBlank(packageConfigProperties.xml)? defaultValue.xml:packageConfigProperties.xml;
        packageConfigProperties.mapper = StrUtil.isBlank(packageConfigProperties.mapper)? defaultValue.mapper:packageConfigProperties.mapper;
        packageConfigProperties.serviceImpl = StrUtil.isBlank(packageConfigProperties.serviceImpl)? defaultValue.serviceImpl:packageConfigProperties.serviceImpl;
        packageConfigProperties.service = StrUtil.isBlank(packageConfigProperties.service)? defaultValue.service:packageConfigProperties.service;
        return packageConfigProperties;
    }
}
