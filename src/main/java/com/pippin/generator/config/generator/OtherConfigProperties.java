package com.pippin.generator.config.generator;

import lombok.Data;

/**
 * @author :  Natus
 * @date : 2020/7/27 0027
 * @description : 其他注解
 */
@Data
public class OtherConfigProperties {

    /**
     * 是否开启分页
     */
    private Boolean page = false;

    /**
     * 是否生成控制层文件
     */
    private Boolean controller = false;

    /**
     * 是否生成service层代码
     */
    private Boolean service = false;

    /**
     * 是否生成mapper层代码
     */
    private Boolean mapper = false;

    /**
     * 是否生成实体类
     */
    private Boolean entity = false;

    /**
     * 是否新增insert方法
     */
    private Boolean insert = false;

    /**
     * 是否新增insertSelective方法
     */
    private Boolean insertSelective = false;

    /**
     * 是否新增update方法
     */
    private Boolean update = false;

    /**
     * 是否新增updateSelective方法
     */
    private Boolean updateSelective = false;

    /**
     * 是否新增delete方法
     */
    private Boolean delete = false;

    /**
     * 是否新增delete方法
     */
    private Boolean deleteSelective = false;

    /**
     * 是否新增select方法
     */
    private Boolean select = false;

    /**
     * 是否新增selectListSelective方法（返回List集合）
     */
    private Boolean selectListSelective = false;

    /**
     * 是否新增selectSingleSelective方法（返回单个实体类）
     */
    private Boolean selectSingleSelective = false;

    /**
     * 是否新增count方法（查询条数）
     */
    private Boolean count = false;

    /**
     * 是否做了配置
     */
    private Boolean checkConfig = false;
}
