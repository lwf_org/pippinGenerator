package com.pippin.generator.config.generator;

import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import lombok.Data;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
@Data
public class StrategyConfigProperties {

    /**
     * 是否大写命名
     */
    private boolean capitalMode = false;

    /**
     * 是否跳过视图
     */
    private boolean skipView = false;

    /**
     * 表名
     */
    private String[] include = null;

    /**
     * 【实体】是否为lombok模型（默认 false）<br>
     */
    private boolean entityLombokModel = true;

    /**
     * 表前缀
     */
    private String[] tablePrefix;

    /**
     * 字段前缀
     */
    private String[] fieldPrefix;

    /**
     * 数据库表映射到实体的命名策略
     */
    private NamingStrategy naming = NamingStrategy.underline_to_camel;

    /**
     * 生成 <code>@RestController</code> 控制器
     * <pre>
     *      <code>@Controller</code> -> <code>@RestController</code>
     * </pre>
     */
    private boolean restControllerStyle = false;

    public StrategyConfig built(String include) {
        return new StrategyConfig()
                .setCapitalMode(this.capitalMode)
                .setInclude(include.split(","))
                .setRestControllerStyle(restControllerStyle)
                .setEntityLombokModel(entityLombokModel)
                .setTablePrefix(tablePrefix)
                .setFieldPrefix(fieldPrefix)
                .setSkipView(skipView)
                .setNaming(naming);
    }
}
