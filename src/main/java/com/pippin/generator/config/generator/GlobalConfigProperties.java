package com.pippin.generator.config.generator;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import lombok.Data;

/**
 * @author : Natus
 * @date : 2020/7/27 0027
 */
@Data
public class GlobalConfigProperties {

    /**
     * 生成文件的输出目录【默认 D 盘根目录】
     */
    private String outputDir = "D:/pippin/";

    /**
     * 是否覆盖已有文件
     */
    private boolean fileOverride = true;

    /**
     * 是否打开输出目录
     */
    private boolean open = true;

    /**
     * 是否在xml中添加二级缓存配置
     */
    private boolean enableCache = false;

    /**
     * 开发人员
     */
    private String author = "pippin";

    /**
     * 开启 Kotlin 模式
     */
    private boolean kotlin = false;

    /**
     * 开启 swagger2 模式
     */
    private boolean swagger2 = false;

    /**
     * 开启 ActiveRecord 模式
     */
    private boolean activeRecord = false;

    /**
     * 开启 BaseResultMap
     */
    private boolean baseResultMap = false;

    /**
     * 时间类型对应策略
     */
    private DateType dateType = DateType.ONLY_DATE;

    /**
     * 开启 baseColumnList
     */
    private boolean baseColumnList = false;

    /**
     * 各层文件名称方式，例如： %sAction 生成 UserAction
     * %s 为占位符
     */
    private String entityName;
    private String mapperName = "%sMapper";
    private String xmlName = "%sMapper";
    private String serviceName = "%sService";
    private String serviceImplName = "%sServiceImpl";
    private String controllerName = "%sController";
    /**
     * 指定生成的主键的ID类型
     */
    private IdType idType;


    public GlobalConfig built(){
        return new GlobalConfig()
                .setDateType(this.dateType)
                .setActiveRecord(this.activeRecord)
                .setAuthor(this.author)
                .setBaseColumnList(this.baseColumnList)
                .setBaseResultMap(this.baseResultMap)
                .setKotlin(kotlin)
                .setEnableCache(enableCache)
                .setFileOverride(fileOverride)
                .setOpen(open)
                .setOutputDir(outputDir)
                .setSwagger2(swagger2)
                .setControllerName(controllerName)
                .setServiceName(serviceName)
                .setServiceImplName(serviceImplName)
                .setMapperName(mapperName)
                .setXmlName(xmlName)
                .setEntityName(entityName)
                .setIdType(idType);

    }

    public static GlobalConfigProperties change(GlobalConfigProperties globalConfigProperties){
        GlobalConfigProperties defaultValue = new GlobalConfigProperties();
        globalConfigProperties.outputDir =  StrUtil.isBlank(globalConfigProperties.outputDir)?defaultValue.outputDir:globalConfigProperties.outputDir;
        globalConfigProperties.author =  StrUtil.isBlank(globalConfigProperties.author)?defaultValue.author:globalConfigProperties.author;
        globalConfigProperties.mapperName =  StrUtil.isBlank(globalConfigProperties.mapperName)?defaultValue.mapperName:globalConfigProperties.mapperName;
        globalConfigProperties.xmlName =  StrUtil.isBlank(globalConfigProperties.xmlName)?defaultValue.xmlName:globalConfigProperties.xmlName;
        globalConfigProperties.serviceName =  StrUtil.isBlank(globalConfigProperties.serviceName)?defaultValue.serviceName:globalConfigProperties.serviceName;
        globalConfigProperties.serviceImplName =  StrUtil.isBlank(globalConfigProperties.serviceImplName)?defaultValue.serviceImplName:globalConfigProperties.serviceImplName;
        globalConfigProperties.controllerName =  StrUtil.isBlank(globalConfigProperties.controllerName)?defaultValue.controllerName:globalConfigProperties.controllerName;
        return globalConfigProperties;
    }
}
