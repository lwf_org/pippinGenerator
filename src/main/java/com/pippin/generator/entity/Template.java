package com.pippin.generator.entity;

import com.pippin.generator.config.generator.GlobalConfigProperties;
import com.pippin.generator.config.generator.OtherConfigProperties;
import com.pippin.generator.config.generator.PackageConfigProperties;
import com.pippin.generator.config.generator.StrategyConfigProperties;
import lombok.Data;

/**
 * @author :  Natus
 * @date : 2020/7/27 0027
 * @description :
 */
@Data
public class Template {

    public String include;

    public String hashCode;

    public OtherConfigProperties otherConfig = new OtherConfigProperties();

    public GlobalConfigProperties globalConfig = new GlobalConfigProperties();

    public PackageConfigProperties packageConfig = new PackageConfigProperties();

    public StrategyConfigProperties strategyConfig = new StrategyConfigProperties();

}
