package com.pippin.generator.entity;

import lombok.Data;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
@Data
public class Page {


    private Integer current = 1;


    private Integer size = 30;

}
