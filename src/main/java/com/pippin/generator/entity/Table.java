package com.pippin.generator.entity;

import lombok.Data;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
@Data
public class Table {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 备注
     */
    private String comment;
}
