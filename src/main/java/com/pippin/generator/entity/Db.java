package com.pippin.generator.entity;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import lombok.Data;

import java.util.Objects;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
@Data
public class Db extends Page {

    private String driverName;

    private String userName;

    private String password;

    private String url;

    private String database;

    private String hashCode;

    public Db build() {
        String[] split = this.url.split("\\?");
        String[] split1 = split[0].split("/");
        this.database = split1[split1.length - 1];
        return this;
    }

    public DataSourceConfig built() {
        return new DataSourceConfig()
                .setDbType(DbType.MYSQL)
                .setDriverName(driverName)
                .setPassword(password)
                .setUsername(userName)
                .setUrl(url);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Db db = (Db) o;
        return Objects.equals(driverName, db.driverName) &&
                Objects.equals(userName, db.userName) &&
                Objects.equals(password, db.password) &&
                Objects.equals(url, db.url) &&
                Objects.equals(database, db.database);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), driverName, userName, password, url, database);
    }
}
