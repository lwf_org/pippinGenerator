package com.pippin.generator.service.impl;

import cn.hutool.db.Entity;
import cn.hutool.db.ds.simple.SimpleDataSource;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import com.pippin.generator.config.generator.MapConfigProperties;
import com.pippin.generator.entity.Db;
import com.pippin.generator.entity.Table;
import com.pippin.generator.service.TableService;
import com.pippin.generator.util.HttpResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
@Slf4j
@Service
public class TableServiceImpl implements TableService {

    private static Map<String, List<Table>> tableMap = new HashMap<>();

    @Override
    public HttpResult getTablePage(Db dbProperties) {
        List<Table> list;
        if (dbProperties.getHashCode() != null) {
            list = tableMap.get(dbProperties.getHashCode());
            if (list == null) {
                return HttpResult.error();
            }
        } else {
            list = getTable(dbProperties.build());
        }
        List<Table> collect = list.stream().skip((dbProperties.getCurrent() - 1) * dbProperties.getSize()).limit(dbProperties.getSize()).collect(Collectors.toList());
        Map<Object, Object> list1 = new HashMap<>(2);
        list1.put("tables", collect);
        list1.put("count", list.size());
        list1.put("hashCode", dbProperties.hashCode());
        return HttpResult.ok(list1);
    }

    private List<Table> getTable(Db dbProperties) {
        DataSource ds = new SimpleDataSource(dbProperties.getUrl(), dbProperties.getUserName(), dbProperties.getPassword(), dbProperties.getDriverName());

        List<Table> list = new LinkedList<>();
        try (Connection conn = ds.getConnection()) {
            /* 执行查询语句，返回实体列表，一个Entity对象表示一行的数据，Entity对象是一个继承自HashMap的对象，存储的key为字段名，value为字段值 */
            List<Entity> entityList = SqlExecutor.query(conn, "select TABLE_NAME as tableName,TABLE_COMMENT as tableComment from information_schema.`TABLES` where TABLE_SCHEMA = ?;", new EntityListHandler(), dbProperties.getDatabase());
            entityList.forEach(entity -> {
                Table table = new Table();
                table.setTableName(String.valueOf(entity.get("tableName")));
                table.setComment(String.valueOf(entity.get("tableComment")));
                list.add(table);
            });
            MapConfigProperties.dbMap.put(String.valueOf(dbProperties.hashCode()), dbProperties);
            MapConfigProperties.initProperties(String.valueOf(dbProperties.hashCode()));
            tableMap.put(String.valueOf(dbProperties.hashCode()), list);
            log.info("{}", entityList);
        } catch (SQLException e) {
            log.error("SQL error!", e);
        }
        return list;
    }

    @Override
    public void removeMap(String hashCode) {
        tableMap.remove(hashCode);
    }

    @Override
    public boolean checkMap(String hashCode) {
        return tableMap.containsKey(hashCode);
    }
}
