package com.pippin.generator.service;

import com.pippin.generator.entity.Db;
import com.pippin.generator.util.HttpResult;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
public interface TableService {

    /**
     * 分页查询数据表
     * @param dbProperties 参数
     * @return HttpResult
     */
    HttpResult getTablePage(Db dbProperties);

    /**
     * 删除表缓存
     * @param hashCode
     */
    void removeMap(String hashCode);

    /**
     * 校验是否修改过配置
     * @param hashCode
     * @return
     */
    boolean checkMap(String hashCode);

}
