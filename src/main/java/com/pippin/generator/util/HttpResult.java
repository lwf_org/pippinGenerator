package com.pippin.generator.util;

import lombok.Data;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description :
 */
@Data
public class HttpResult {

    private String code = "00000";

    private String msg;

    private Object data;

    public HttpResult() {
    }

    /**
     * 准守阿里错误码
     * @param code
     * @param msg
     * @param data
     * @return
     */
    public static HttpResult error(String code, String msg, Object data){
        HttpResult httpResult = new HttpResult();
        httpResult.setCode(code);
        httpResult.setMsg(msg);
        httpResult.setData(data);
        return httpResult;
    }



    public static HttpResult error(String msg, Object data){
        HttpResult httpResult = new HttpResult();
        httpResult.setCode("500");
        httpResult.setMsg(msg);
        httpResult.setData(data);
        return httpResult;
    }

    public static HttpResult error(Object object){
        HttpResult httpResult = new HttpResult();
        httpResult.setCode("500");
        httpResult.setData(object);
        return httpResult;
    }

    public static HttpResult error(){
        HttpResult httpResult = new HttpResult();
        httpResult.setCode("500");
        return httpResult;
    }

    public static HttpResult error(String msg){
        HttpResult httpResult = new HttpResult();
        httpResult.setCode("500");
        httpResult.setMsg(msg);
        return httpResult;
    }

    public static HttpResult error(String code, String msg){
        HttpResult httpResult = new HttpResult();
        httpResult.setCode(code);
        httpResult.setMsg(msg);
        return httpResult;
    }

    public static HttpResult ok(){
        return new HttpResult();
    }

    public static HttpResult ok(Object object){
        HttpResult httpResult = new HttpResult();
        httpResult.setData(object);
        return httpResult;
    }

    public static HttpResult ok(String msg, Object data){
        HttpResult httpResult = new HttpResult();
        httpResult.setMsg(msg);
        httpResult.setData(data);
        return httpResult;
    }

}
