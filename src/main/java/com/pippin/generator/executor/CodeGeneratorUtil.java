package com.pippin.generator.executor;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.engine.BeetlTemplateEngine;
import com.pippin.generator.config.beetl.BeetlTemplateEngineV2;
import com.pippin.generator.config.generator.MapConfigProperties;
import com.pippin.generator.config.generator.OtherConfigProperties;
import com.pippin.generator.entity.Template;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author :  Natus
 * @date : 2020/7/27 0027
 * @description : 其他注解
 */
public class CodeGeneratorUtil {

    public static void createTemplate(Template template) {



        AutoGenerator mpg = new AutoGenerator()
                .setGlobalConfig(MapConfigProperties.globalMap.get(template.hashCode).built())
                .setDataSource(MapConfigProperties.dbMap.get(template.hashCode).built())
                .setStrategy(MapConfigProperties.strategyMap.get(template.hashCode).built(template.include))
                .setPackageInfo(MapConfigProperties.packageMap.get(template.hashCode).built())
                .setTemplateEngine(new BeetlTemplateEngineV2());

        OtherConfigProperties otherConfigProperties = MapConfigProperties.otherMap.get(template.hashCode);
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> objectMap = BeanUtil.beanToMap(otherConfigProperties);
                this.setMap(objectMap);
            }
        };


        List<FileOutConfig> focList = new ArrayList<>();

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 自定义模板配置
        TemplateConfig tc = new TemplateConfig();
        if (otherConfigProperties.getController()) {
            tc.setController("/templates/controller.java");
        } else {
            tc.setController(null);
        }

        if (otherConfigProperties.getService()) {
            tc.setService("/templates/service.java");
            tc.setServiceImpl("/templates/serviceImpl.java");
        } else {
            tc.setService(null);
            tc.setServiceImpl(null);
        }

        if (otherConfigProperties.getMapper()) {
            tc.setXml("/templates/mapper.xml");
            tc.setMapper("/templates/mapper.java");
        } else {
            tc.setXml(null);
            tc.setMapper(null);
        }

        if (otherConfigProperties.getEntity()) {
            tc.setEntity("/templates/entity.java");
        } else {
            tc.setEntity(null);
        }
        mpg.setTemplate(tc);

        System.out.println("***********文件生成开始**************");

        // 执行生成
        mpg.execute();

        System.out.println("***********文件生成完成**************");

    }

}
