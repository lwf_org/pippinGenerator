package com.pippin.generator.controller;

import com.pippin.generator.service.TableService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

/**
 * @author :  Natus
 * @date : 2020/7/28 0028
 * @description :
 */
@Controller
public class WebController {


    @Resource
    private TableService tableService;

    /**
     * 跳转菜单主页
     *
     * @return String
     */
    @RequestMapping(value = {"/", "/index"})
    public String index(@RequestParam(required = false) String hashCode) {
        if(hashCode != null){
            tableService.removeMap(hashCode);
        }
        return "connect.html";
    }

    /**
     * 跳转菜单主页
     *
     * @return String
     */
    @RequestMapping("creat")
    public String creat(Model model, @RequestParam String hashCode) {
        model.addAttribute("hashCode",hashCode);
        return "creat.html";
    }

    /**
     * 跳转菜单主页
     *
     * @return String
     */
    @RequestMapping("config")
    public String config(Model model, @RequestParam String hashCode) {
        model.addAttribute("hashCode",hashCode);
        return "config.html";
    }


}
