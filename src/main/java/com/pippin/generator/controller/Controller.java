package com.pippin.generator.controller;

import com.pippin.generator.config.generator.GlobalConfigProperties;
import com.pippin.generator.config.generator.MapConfigProperties;
import com.pippin.generator.config.generator.OtherConfigProperties;
import com.pippin.generator.config.generator.PackageConfigProperties;
import com.pippin.generator.entity.Db;
import com.pippin.generator.entity.Template;
import com.pippin.generator.executor.CodeGeneratorUtil;
import com.pippin.generator.service.TableService;
import com.pippin.generator.util.HttpResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author :  Natus
 * @date : 2020/7/24 0024
 * @description : 页面主入口
 */
@RestController
public class Controller {

    @Resource
    private TableService tableService;

    /**
     * 获取数据库数据
     * @param dbProperties
     * @return
     */
    @RequestMapping("getDataBasePage")
    public HttpResult getDataBasePage(@RequestBody Db dbProperties){
        return tableService.getTablePage(dbProperties);
    }

    /**
     * 创建模板
     * @param template
     * @return
     */
    @RequestMapping("createTemplate")
    public HttpResult createTemplate(@RequestBody Template template){
        CodeGeneratorUtil.createTemplate(template);
        return HttpResult.ok();
    }

    /**
     * 校验是否配置
     * @param hashCode
     * @return
     */
    @RequestMapping("checkConfig")
    public HttpResult checkConfig(@RequestParam String hashCode){
        return HttpResult.ok(MapConfigProperties.otherMap.get(hashCode).getCheckConfig());
    }

    /**
     * 创建模板
     * @param template
     * @return
     */
    @RequestMapping("saveConfig")
    public HttpResult saveConfig(@RequestBody Template template){
        template.otherConfig.setCheckConfig(true);
        MapConfigProperties.otherMap.put(template.hashCode, template.otherConfig);
        MapConfigProperties.strategyMap.put(template.hashCode, template.strategyConfig);
        MapConfigProperties.packageMap.put(template.hashCode, PackageConfigProperties.change(template.packageConfig));
        MapConfigProperties.globalMap.put(template.hashCode, GlobalConfigProperties.change(template.globalConfig));
        return HttpResult.ok();
    }

}
