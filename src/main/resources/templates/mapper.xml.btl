<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${table.mapperName}">


    <% if(baseResultMap) { %>
    <!-- 通用查询结果列 -->
    <resultMap id="BaseResultMap" type="${package.Entity}.${entity}">
        <% for(field in table.fields){ %>
            <% if(field.keyFlag){ %>
                <id column="${field.name}" property="${field.propertyName}"/>
            <% } %>
        <% } %>
        <% for(field in table.commonFields){ %>
           <% if(!field.keyFlag){ %>
                <result column="${field.name}" property="${field.propertyName}"/>
           <% } %>
        <% } %>
         <% for(field in table.fields){ %>
           <% if(!field.keyFlag){ %>
                <result column="${field.name}" property="${field.propertyName}"/>
           <% } %>
        <% } %>
    </resultMap>
    <% } %>

    <% if(baseColumnList) { %>
    <!-- 通用查询查询列 -->
    <sql id="Base_Column_List">
    ${table.fieldNames}
    </sql>
    <% } %>

<% if(cfg.selectSingleSelective){ %>
    <!-- 条件查询返回实体 -->
    <select id="selectSingleSelective" parameterType="${package.Entity}.${entity}" resultMap="BaseResultMap">
        select
        <include refid="Base_Column_List" />
        from ${table.name}
        <where>
            <% for(field in table.fields){ %>
            <if test="${field.propertyName} != null and ${field.propertyName} != ''">
               and ${field.name} = #{${field.propertyName}}
            </if>
            <% } %>
        </where>
    </select>
<% } %>

<% if(cfg.selectListSelective){ %>
    <!-- 条件查询返回实体集合 -->
    <select id="selectListSelective" parameterType="${package.Entity}.${entity}" resultMap="BaseResultMap">
        select
        <include refid="Base_Column_List" />
        from ${table.name}
        <where>
            <% for(field in table.fields){ %>
            <if test="${field.propertyName} != null and ${field.propertyName} != ''">
               and ${field.name} = #{${field.propertyName}}
            </if>
            <% } %>
        </where>
    </select>
<% } %>

<% if(cfg.insert){ %>
    <!-- 添加 -->
    <insert id="insert${entity}" parameterType="${package.Entity}.${entity}">
        insert into ${table.name}
        (<% for(field in table.fields){ if(!field.keyFlag){ %>${field.name}<% }if(!field.keyFlag && !fieldLP.last){%>,<%}}%>)
        values
        (<% for(field in table.fields){ if(!field.keyFlag){ %>#{${field.propertyName}}<% }if(!field.keyFlag && !fieldLP.last){%>,<%}}%>)
    </insert>
<% } %>

<% if(cfg.insertSelective){ %>
     <!-- 条件添加 -->
    <insert id="insert${entity}Selective" parameterType="${package.Entity}.${entity}">
        insert into ${table.name}
         <trim prefix="(" suffix=")" suffixOverrides=",">
        <% for(field in table.fields){ %>
           <% if(!field.keyFlag){ %>
            <if test="${field.propertyName} != null">
                ${field.name},
            </if>
           <% } %>
       <% } %>
       </trim>
       <trim prefix="values (" suffix=")" suffixOverrides=",">
        <% for(field in table.fields) { %>
            <% if(!field.keyFlag){ %>
            <if test="${field.propertyName} != null">
                #{${field.propertyName}},
            </if>
            <% } %>
        <% } %>
        </trim>
    </insert>
<% } %>

<% if(cfg.updateSelective){ %>
    <!-- 根据主键更新 -->
    <update id="update${entity}Selective" parameterType="${package.Entity}.${entity}">
        update ${table.name}
        <set>
         <% for(field in table.fields){ %>
            <% if(!field.keyFlag){ %>
            <if test="${field.propertyName} != null">
                ${field.name} = #{${field.propertyName}},
            </if>
            <% } %>
         <% } %>
        </set>
        <% for(field in table.fields){ %>
        <% if(field.keyFlag){ %>
        where ${field.name} = #{${field.propertyName}}
        <% } %>
        <% } %>
    </update>
<% } %>

<% if(cfg.deleteSelective){ %>
    <!-- 条件删除 -->
    <delete id="delete${entity}Selective" parameterType="${package.Entity}.${entity}">
        delete from ${table.name}
        <where>
        <% for(field in table.fields){ %>
            <if test="${field.propertyName} != null">
               and ${field.name} = #{${field.propertyName}}
            </if>
        <% } %>
        </where>
    </delete>
<% } %>

<% if(cfg.count){ %>
    <!-- 查询总数 -->
    <select id="count" >
        select count(1) from ${table.name};
    </select>
<% } %>
</mapper>
