﻿// 以下代码是配置layui扩展模块的目录，每个页面都需要引入
layui.config({
    base: '/static/' //假设这是你存放拓展模块的根目录
}).extend({ //设定模块别名
    ax: "ax/ax"
});